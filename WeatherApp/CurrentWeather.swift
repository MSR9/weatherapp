//
//  CurrentWeather.swift
//  WeatherApp
//
//  Created by PrahladReddy on 1/16/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import Foundation

class CurrentWeather {
    let temperature: Double?
    let windSpeed : Double?
    let humidity: Double?
    let precipProbability: Double?
    let summary: String?
    
    struct WeatherKeys {
        static let temperature = "temperature"
        static let windSpeed = "windSpeed"
        static let  humidity = "humidity"
        static let precipProbability = "precipProbability"
        static let summary = "summary"
    }
    
    init(weatherDictionary: [String:Any]) {
        self.temperature = weatherDictionary[WeatherKeys.temperature] as? Double
        self.windSpeed = weatherDictionary[WeatherKeys.windSpeed] as? Double
        
        if let humidityDouble = weatherDictionary[WeatherKeys.humidity] as? Double {
            self.humidity = Double(humidityDouble * 100)
        } else {
            self.humidity = nil
        }
        
        if let precipProbabilityDouble = weatherDictionary[WeatherKeys.precipProbability] as? Double {
            self.precipProbability = Double(precipProbabilityDouble * 100)
        } else {
            self.precipProbability = nil
        }
        self.summary = weatherDictionary[WeatherKeys.summary] as? String
    }
}
