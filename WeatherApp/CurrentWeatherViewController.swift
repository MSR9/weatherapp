//
//  CurrentWeatherViewController.swift
//  WeatherApp
//
//  Created by EPITADMBP04 on 1/16/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit

class CurrentWeatherViewController: UIViewController {
    
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var celciusFarenheit: UILabel!
    
    let forcastAPIKey = "53d9ba8ff0fe453574e2e5b7ac951c86"
    let coordinate: (lat: Double, lon: Double) = (40.213280, -77.008034)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let forecastService = ForecastService(APIKey: forcastAPIKey)
        forecastService.getforecast(latitude: coordinate.lat, longitude: coordinate.lon) { (currentWeather) in
            
            if let currentWeather =  currentWeather {
                DispatchQueue.main.async {
                    if let temperature = currentWeather.temperature {
                        self.temperature.text = "\(temperature)"
                    } else {
                        self.temperature.text = "-"
                    }
                }
                
                DispatchQueue.main.async {
                    if let celsius = currentWeather.windSpeed {
                        self.celciusFarenheit.text = "\(celsius)"
                    } else {
                        self.celciusFarenheit.text = "-"
                    }
                }
                
            }
        }
    }
}
