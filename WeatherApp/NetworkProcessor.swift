//
//  NetworkProcessor.swift
//  WeatherApp
//
//  Created by PrahladReddy on 1/15/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import Foundation

class NetworkProcessor {
    lazy var session = URLSession(configuration: URLSessionConfiguration.default)
    
    let url : URL
    
    init(url: URL) {
        self.url = url
    }
    
    typealias JSONDictionaryHandler = (([String: Any]?) -> Void)
    
    func downloadJSONfromURL(_ completion: @escaping (([String: Any]?) -> Void)) {
        let request = URLRequest(url: self.url)
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            if error == nil {
                if let httpResponse = response as? HTTPURLResponse {
                    switch httpResponse.statusCode {
                    case 200:
                        // Successful Response
                        if let data = data {
                            do {
                                let jsonDictionary = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                                completion(jsonDictionary as? [String:Any])
                            } catch let error as NSError {
                                print("Error processing json data: \(error.localizedDescription)")
                            }
                        }
                    default:
                        print("HTTP Response code: \(httpResponse.statusCode)")
                    }
                }
            } else {
                print("Error: \(error?.localizedDescription)")
            }
        }
        dataTask.resume()
    }
}
