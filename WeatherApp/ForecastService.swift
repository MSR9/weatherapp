//
//  ForecastService.swift
//  WeatherApp
//
//  Created by EPITADMBP04 on 1/15/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import Foundation

class ForecastService {
    let forecastAPIKey : String
    let forecastBaseURL : URL?
    
    // Format of URL
    // forecastBaseURL/forecastAPIKey/latitude/longitude
    
    init(APIKey : String) {
        self.forecastAPIKey = APIKey
        forecastBaseURL = URL(string: "https://api.darksky.net/forecast/\(APIKey)")
    }
    
    func getforecast(latitude: Double, longitude: Double, completion: @escaping (CurrentWeather?) -> Void) {
        if let forecastURL = URL(string: "\(forecastBaseURL!)/\(latitude),\(longitude)") {
            
            let networkProcessor = NetworkProcessor(url: forecastURL)
            networkProcessor.downloadJSONfromURL { (jsonDictionary) in
                // TODO: SOME HOW PARSE jsonDictionary INTO A SWIFT WEATHER OBJECT
                if let currentWeatherDictionary = jsonDictionary?["currently"] as? [String: Any] {
                    let currentweather = CurrentWeather.init(weatherDictionary: currentWeatherDictionary)
                    completion(currentweather)
                }
            }
        }
    }
}
